# Gopwned CLI #



### What is this repository for? ###

A command line interface **CLI** to search for a email account in the [**PWNED**](https://haveibeenpwned.com/)
API and determine if it has been compromised in a security breach.

### How do I get set up? ###

*   Clone the repository in the src directory of the go configuration
